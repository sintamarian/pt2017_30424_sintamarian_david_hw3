package model;

public class Order {
	private int id;
	private int customer_id;
	private int price;
	public Order(int id, int customer_id, int price){
		super();
		this.id = id;
		this.customer_id = customer_id;
		this.price = price;
	}
	public Order(int customer_id, int price){
		super();
		this.customer_id = customer_id;
		this.price = price;
	}
	public void setId(int id){
		this.id = id;
	}
	public int getId(){
		return this.id;
	}
	public int getCustomerId(){
		return this.customer_id;
	}
	public int getPrice(){
		return this.price;
	}
}
