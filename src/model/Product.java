package model;

public class Product {
	private int id;
	private String name;
	private int price;
	private int quantity;
	public Product(int id, String name, int price, int quantity){
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		this.quantity = quantity;
	}
	public Product(String name, int price, int quantity){
		super();
		this.name = name;
		this.price = price;
		this.quantity = quantity;
	}
	public void setId(int id){
		this.id = id;
	}
	public int getId(){
		return this.id;
	}
	public String getName(){
		return this.name;
	}
	public int getPrice(){
		return this.price;
	}
	public int getQuantity(){
		return this.quantity;
	}
}
