package model;

public class Customer {
	private int id;
	private String name;
	private String address;
	private String phone_number;
	private String email;
	public Customer(){
		
	}
	public Customer(int id, String name, String address, String phone_number, String email){
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.phone_number = phone_number;
		this.email = email;
	}
	public Customer(String name, String address, String phone_number, String email){
		super();
		this.name = name;
		this.address = address;
		this.phone_number = phone_number;
		this.email = email;
	}
	public int getId(){
		return this.id;
	}
	public void setId(int id){
		this.id = id;
	}
	public String getName(){
		return this.name;
	}
	public String getAddress(){
		return this.address;
	}
	public String getPhone(){
		return this.phone_number;
	}
	public String getEmail(){
		return this.email;
	}
}
