package model;

public class Warehouse_product {
	private int id;
	private int product_id;
	private int quantity;
	public Warehouse_product(int id, int product_id, int quantity){
		this.id = id;
		this.product_id = product_id;
		this.quantity = quantity;
	}
	public Warehouse_product(int product_id, int quantity){
		this.product_id = product_id;
		this.quantity = quantity;
	}
	public void setId(int id){
		this.id = id;
	}
	public int getId(){
		return this.id;
	}
	public int getProductId(){
		return this.product_id;
	}
	public int getQuantity(){
		return this.quantity;
	}
}
