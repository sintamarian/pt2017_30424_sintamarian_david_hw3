package model;

public class Product_order {
	private int id;
	private int order_id;
	private int product_id;
	private int quantity;
	public Product_order(int id, int order_id, int product_id, int quantity){
		super();
		this.id = id;
		this.order_id = order_id;
		this.product_id = product_id;
		this.quantity = quantity;
	}
	public Product_order(int order_id, int product_id, int quantity){
		super();
		this.order_id = order_id;
		this.product_id = product_id;
		this.quantity = quantity;
	}
	public void setId(int id){
		this.id = id;
	}
	public int getId(){
		return this.id;
	}
	public int getProductId(){
		return this.product_id;
	}
	public int getOrderId(){
		return this.order_id;
	}
	public int getQuantity(){
		return this.quantity;
	}
}
