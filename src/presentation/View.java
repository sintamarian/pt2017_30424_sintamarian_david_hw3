package presentation;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextArea;

import dao.CustomerDAO;
import model.Customer;

public class View {
	
	private Controller controller;
	private String[] customersString = null;
	
	public View(){
		controller = new Controller();
		JFrame main = new JFrame();
		main.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		main.setTitle("Menu");
		main.setSize(300, 250);
		main.setLocationRelativeTo(null);
		JPanel menuButtons = new JPanel();
		JButton manageCustomers = new JButton("Manage Customers");
		JButton manageProducts = new JButton("Manage Products");
		JButton manageOrders = new JButton("Manage Orders");
		menuButtons.add(manageCustomers);
		menuButtons.add(manageProducts);
		menuButtons.add(manageOrders);
		main.add(menuButtons);
		main.setVisible(true);
		manageOrders.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				manageOrders();
			}
		});
		manageCustomers.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				customersString = controller.getCustomersDropDown();
				manageCustomers();
			}
		});
		manageProducts.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
//				customersString = controller.getCustomersDropDown();
				manageProducts();
			}
		});
	}
	public void manageOrders(){
		GridLayout layout = new GridLayout(0, 2);
		ArrayList<JComboBox> listProd = new ArrayList<JComboBox>();
		ArrayList<JTextArea> listQty = new ArrayList<JTextArea>();
		
		JFrame frameOrder = new JFrame();
		frameOrder.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frameOrder.setTitle("Order");
		frameOrder.setSize(300, 250);
		frameOrder.setLocationRelativeTo(null);
		JPanel customerPanel = new JPanel();
		customerPanel.setLayout(layout);
		JLabel customerLab = new JLabel("Customer:");
		String[] customers = controller.getCustomersDropDown();
		String[] products = controller.getProductsDropDown();
		JComboBox customer = new JComboBox(customers);
		customerPanel.add(customerLab);
		customerPanel.add(customer);
		frameOrder.add(customerPanel, BorderLayout.NORTH);
		
		JPanel productPanel = new JPanel();
		productPanel.setLayout(layout);
		JLabel name = new JLabel("Product");
		JLabel qty = new JLabel("Quantity");
		productPanel.add(name);
		productPanel.add(qty);
		JComboBox product = new JComboBox(products);
		JTextArea quantity = new JTextArea();
		productPanel.add(product);
		listProd.add(product);
		productPanel.add(quantity);
		listQty.add(quantity);
		frameOrder.add(productPanel, BorderLayout.CENTER);
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(layout);
		JButton add = new JButton("Add");
		JButton order = new JButton("Order");
		buttonPanel.add(add);
		buttonPanel.add(order);
		frameOrder.add(buttonPanel, BorderLayout.SOUTH);
		
		frameOrder.setVisible(true);
		add.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				JComboBox product = new JComboBox(products);
				listProd.add(product);
				JTextArea quantity = new JTextArea();
				listQty.add(quantity);
				productPanel.add(product);
				productPanel.add(quantity);
				productPanel.revalidate();
				productPanel.repaint();
			}
		});
		order.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				String customerName = (String) customer.getSelectedItem();
				String productName;
				int productQuantity;
				int totalPrice = 0;
				boolean ok = true;
				for(int i = 0; i<listProd.size();++i){
					productName = (String) listProd.get(i).getSelectedItem();
					productQuantity = Integer.parseInt(listQty.get(i).getText());
					int price = controller.checkStock(productName, productQuantity);
					if(price == 0){
						controller.errorFrame("Product "+productName+" is not available in the desired quantity!");
						ok = false;
					}else{
						totalPrice+= price;
					}
					
				}
				if(ok == true){
					try{
					
						System.out.println(totalPrice);
						PrintWriter writer = new PrintWriter("Order-"+customerName);
						writer.println("Customer: "+customerName);
						writer.println("Total Price: "+totalPrice);
						int orderId = controller.createOrder(customerName, totalPrice);
						for(int i = 0; i<listProd.size();++i){
							productName = (String) listProd.get(i).getSelectedItem();
							productQuantity = Integer.parseInt(listQty.get(i).getText());
							writer.println(productName + " - " + productQuantity);
							controller.addProductsToOrder(orderId, productName, productQuantity);
						}
						writer.close();
					}
					catch(Exception ex){
						
					}
				}
			}
		});
	}
	
	public void manageCustomers(){
		GridLayout layout = new GridLayout(0, 2);
		
		JFrame frameOrder = new JFrame();
		frameOrder.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frameOrder.setTitle("Manage Customers");
		frameOrder.setSize(300, 250);
		frameOrder.setLocationRelativeTo(null);
		String[] customers = controller.getCustomersDropDown();
		final JPanel Operations = new JPanel();
		Operations.setLayout(layout);
		JComboBox customersComboBox = new JComboBox(customers);
		JButton delete = new JButton("Delete Customer");
		JComboBox editCustomers = new JComboBox(customers);
		JButton edit = new JButton("Edit Customer");
		JLabel name = new JLabel("Name:");
		JTextArea nameText = new JTextArea();
		JLabel address = new JLabel("Address:");
		JTextArea addressText = new JTextArea();
		JLabel email = new JLabel("Email:");
		JTextArea emailText = new JTextArea();
		JLabel phone = new JLabel("Phone:");
		JTextArea phoneText = new JTextArea();
		JButton insert = new JButton("Add New");
		Operations.add(customersComboBox);
		Operations.add(delete);
		Operations.add(editCustomers);
		Operations.add(edit);
		Operations.add(name);
		Operations.add(nameText);
		Operations.add(address);
		Operations.add(addressText);
		Operations.add(email);
		Operations.add(emailText);
		Operations.add(phone);
		Operations.add(phoneText);
		Operations.add(insert);
		JTable customersTable = controller.createCustomersTable(); 
		frameOrder.add(Operations, BorderLayout.NORTH);
		frameOrder.add(customersTable, BorderLayout.CENTER);
		
		frameOrder.setVisible(true);
		insert.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				String name = nameText.getText();
				String address = addressText.getText();
				String email = emailText.getText();
				String phone = phoneText.getText();
				controller.addCustomer(name, address, email, phone);
				frameOrder.dispose();
			}
		});
		delete.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				String name = (String) customersComboBox.getSelectedObjects()[0];
				controller.deleteCustomer(name);
				frameOrder.dispose();
			}
		});
		edit.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				String name = (String) editCustomers.getSelectedObjects()[0];
				editCustomers(name);
				frameOrder.dispose();
			}
		});
	}
	
	public void editCustomers(String customerName){
		GridLayout layout = new GridLayout(0, 2);
		
		JFrame frameCustomer = new JFrame();
		frameCustomer.setTitle("Edit customer: "+customerName);
		frameCustomer.setSize(300, 250);
		frameCustomer.setLocationRelativeTo(null);
		final JPanel Operations = new JPanel();
		Operations.setLayout(layout);
		JLabel name = new JLabel("Name:");
		JTextArea nameText = new JTextArea();
		JLabel address = new JLabel("Address:");
		JTextArea addressText = new JTextArea();
		JLabel email = new JLabel("Email:");
		JTextArea emailText = new JTextArea();
		JLabel phone = new JLabel("Phone:");
		JTextArea phoneText = new JTextArea();
		JButton edit = new JButton("Edit");
		Operations.add(name);
		Operations.add(nameText);
		Operations.add(address);
		Operations.add(addressText);
		Operations.add(email);
		Operations.add(emailText);
		Operations.add(phone);
		Operations.add(phoneText);
		Operations.add(edit);
		
		frameCustomer.add(Operations);
		
		frameCustomer.setVisible(true);
		edit.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				String name = nameText.getText();
				String address = addressText.getText();
				String email = emailText.getText();
				String phone = phoneText.getText();
				controller.editCustomer(customerName, name, address, email, phone);
				frameCustomer.dispose();
			}
		});
	}
	
	public void manageProducts(){
		GridLayout layout = new GridLayout(0, 2);
		
		JFrame frameProduct = new JFrame();
		frameProduct.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frameProduct.setTitle("Manage Products");
		frameProduct.setSize(300, 250);
		frameProduct.setLocationRelativeTo(null);
		String[] products = controller.getProductsDropDown();
		final JPanel Operations = new JPanel();
		Operations.setLayout(layout);
		JComboBox productsComboBox = new JComboBox(products);
		JButton delete = new JButton("Delete Product");
		JComboBox editProducts = new JComboBox(products);
		JButton edit = new JButton("Edit Product");
		JLabel name = new JLabel("Name:");
		JTextArea nameText = new JTextArea();
		JLabel price = new JLabel("Price:");
		JTextArea priceText = new JTextArea();
		JLabel quantity = new JLabel("Quantity:");
		JTextArea quantityText = new JTextArea();
		JButton insert = new JButton("Add New");
		Operations.add(productsComboBox);
		Operations.add(delete);
		Operations.add(editProducts);
		Operations.add(edit);
		Operations.add(name);
		Operations.add(nameText);
		Operations.add(price);
		Operations.add(priceText);
		Operations.add(quantity);
		Operations.add(quantityText);
		Operations.add(insert);
		JTable productsTable = controller.createProductsTable();
		frameProduct.add(Operations, BorderLayout.NORTH);
		frameProduct.add(productsTable, BorderLayout.CENTER);
		
		frameProduct.setVisible(true);
		insert.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				String name = nameText.getText();
				int price = Integer.parseInt(priceText.getText());
				int quantity = Integer.parseInt(quantityText.getText());
				controller.addProduct(name, price, quantity);
				frameProduct.dispose();
			}
		});
		delete.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				String name = (String) productsComboBox.getSelectedObjects()[0];
				controller.deleteProduct(name);
				frameProduct.dispose();
			}
		});
		edit.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				String name = (String) editProducts.getSelectedObjects()[0];
				editProducts(name);
				frameProduct.dispose();
			}
		});
	}

	public void editProducts(String productName){
		GridLayout layout = new GridLayout(0, 2);
		
		JFrame frameProduct = new JFrame();
		frameProduct.setTitle("Edit product: "+productName);
		frameProduct.setSize(300, 250);
		frameProduct.setLocationRelativeTo(null);
		final JPanel Operations = new JPanel();
		Operations.setLayout(layout);
		JLabel n = new JLabel("Name");
		JTextArea nameText = new JTextArea();
		JLabel p = new JLabel("Price");
		JTextArea priceText = new JTextArea();
		JLabel q = new JLabel("Quantity:");
		JTextArea quantityText = new JTextArea();
				
		JButton edit = new JButton("Edit");
		Operations.add(n);
		Operations.add(nameText);
		Operations.add(p);
		Operations.add(priceText);
		Operations.add(q);
		Operations.add(quantityText);
		Operations.add(edit);
		
		frameProduct.add(Operations);
		
		frameProduct.setVisible(true);
		edit.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				String name = nameText.getText();
				int price = 0;//Integer.parseInt(priceText.getText());
				String p;
				String q;
				int quantity = -1;//Integer.parseInt(quantityText.getText());
				p = priceText.getText();
				q = quantityText.getText();
				if(!p.isEmpty()){
					price = Integer.parseInt(p);
				}
				if(!q.isEmpty()){
					quantity = Integer.parseInt(q);
				}
				controller.editProduct(productName, name, price, quantity);
				frameProduct.dispose();
			}
		});
	}
}