package presentation;

import java.lang.reflect.Field;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JTable;

import bll.*;
import dao.CustomerDAO;
import dao.ProductDAO;
import model.Customer;
import model.Product;

public class Controller {

	CustomerBLL cust = new CustomerBLL();
	OrderBLL ord = new OrderBLL();
	ProductOrderBLL prodOrd = new ProductOrderBLL();
	ProductBLL prod = new ProductBLL();
	public int checkStock(String productName, int quantity){
		Product product = ProductDAO.findByName(productName);
		if(quantity > product.getQuantity()){
			return 0;
		}
		return quantity*product.getPrice();
	}
	public void addProduct(String name, int price, int quantity){
		Product product = new Product(name, price, quantity);
		ProductDAO.insertProduct(product);
		return;
	}
	public void deleteProduct(String name){
		ProductDAO.deleteProduct(name);
		return;
	}
	public void editProduct(String prevName, String name, int price, int quantity){
		Product product = ProductDAO.findByName(prevName);
		if(name.isEmpty()){
			name = product.getName();
		}
		if(price == 0){
			price = product.getPrice();
		}
		if(quantity == -1){
			quantity = product.getQuantity();
		}
		int id = product.getId();
		ProductDAO.editProduct(id, name, price, quantity);
	}
	public void addCustomer(String name, String address, String email, String phone){
		Customer customer = new Customer(name, address, phone, email);
		CustomerDAO.insertCustomer(customer);
	}
	public void editCustomer(String prevName, String name, String address, String email, String phone){
		Customer customer = CustomerDAO.findByName(prevName);
		if(name.isEmpty()){
			name = customer.getName();
		}
		if(address.isEmpty()){
			address = customer.getAddress();
		}
		if(email.isEmpty()){
			email = customer.getEmail();
		}
		if(phone.isEmpty()){
			phone = customer.getPhone();
		}
		CustomerDAO.editCustomer(customer.getId(), name, address, email, phone);
	}
	public void deleteCustomer(String name){
		CustomerDAO.deleteCustomer(name);
	}
	public String[] getProductsDropDown(){
		ArrayList<Product> products = ProductDAO.getProducts();
		int size = products.size();
		int i = 0;
		String[] results = new String[size];
		for(Product product : products){
			results[i] = product.getName();
			i++;
		}
		return results;
	}
	public String[] getCustomersDropDown(){
		ArrayList<Customer> customers = CustomerDAO.getCustomers();
		int size = customers.size();
		int i = 0;
		String[] results = new String[size];
		for(Customer customer : customers){
			results[i] = customer.getName();
			i++;
		}
		return results;
	}
	public JTable createCustomersTable(){

		ArrayList<Customer> customers = CustomerDAO.getCustomers();
	
		int size = customers.size(); 
		Object rowData[][] = new Object[size][4];
		Object rowNames[] = {"Name", "Address", "Email", "Phone"};
		int row = 0;
		for(Customer customer : customers){
			rowData[row][0] = customer.getName();
			rowData[row][1] = customer.getAddress();
			rowData[row][2] = customer.getEmail();
			rowData[row][3] = customer.getPhone();
			row++;
		}
		JTable table = new JTable(rowData, rowNames);
		return table;
	}
	
	public JTable createProductsTable(){
		ArrayList<Product> products = ProductDAO.getProducts();
		
		int size = products.size();
		Object rowData[][] = new Object[size][3];
		Object rowNames[] = {"Name", "Price", "Quantity"};
		int row = 0;
		for(Product product : products){
			rowData[row][0] = product.getName();
			rowData[row][1] = product.getPrice();
			rowData[row][2] = product.getQuantity();
			row++;
		}
		JTable table = new JTable(rowData, rowNames);
		return table;
	}
	public int createOrder(String customer, int price){
		Customer customerToInsert = cust.getCustomerByName(customer);
		int id = customerToInsert.getId();
		id = ord.addOrder(id, price);
		return id;
	}
	public void addProductsToOrder(int orderId, String productName, int productQuantity) {
		
		Product product = prod.getProductByName(productName);
		int productId = product.getId();
		editProduct(productName, product.getName(), product.getPrice(), product.getQuantity() - productQuantity);
		prodOrd.addProductOrder(orderId, productId, productQuantity);
	}
	public void errorFrame(String string) {
		// TODO Auto-generated method stub
		JFrame error = new JFrame();
		error.setTitle("Error");
	}
}
