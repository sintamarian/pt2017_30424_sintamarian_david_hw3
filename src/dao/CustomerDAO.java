package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import model.Customer;
import model.Student;

public class CustomerDAO {
	protected static final Logger LOGGER = Logger.getLogger(CustomerDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO customer (name,address,phone_number,email)"
			+ " VALUES (?,?,?,?)";
	private final static String findStatementString = "SELECT * FROM customer where name = ?";
	private final static String getStatementString = "SELECT * FROM customer";
	private final static String deleteStatementString = "DELETE FROM customer where name = ?";
	private final static String editStatementString = "UPDATE customer set name = ?, address = ?, phone_number = ?, email = ? where id = ?";
	public static Customer findByName(String name){
		Customer customer = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setString(1, name);
			rs = findStatement.executeQuery();
			rs.next();

			int id = rs.getInt("id");
			String address = rs.getString("address");
			String email = rs.getString("email");
			String phone_number = rs.getString("phone_number");
			customer = new Customer(id, name, address, phone_number, email);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"StudentDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return customer;
	}
	public static int insertCustomer(Customer customer){
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, customer.getName());
			insertStatement.setString(2, customer.getAddress());
			insertStatement.setString(3, customer.getPhone());
			insertStatement.setString(4, customer.getEmail());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "StudentDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
	public static ArrayList<Customer> getCustomers(){
		ArrayList<Customer> customers = new ArrayList<Customer>();
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(getStatementString);
			rs = findStatement.executeQuery();
			while(rs.next()){
				int id = rs.getInt("id");
				String address = rs.getString("address");
				String email = rs.getString("email");
				String name = rs.getString("name");
				String phone_number = rs.getString("phone_number");
				Customer customer = new Customer(id, name, address, phone_number, email);
				customers.add(customer);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"StudentDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		
		return customers;
	}
	public static void deleteCustomer(String name){
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement deleteStatement = null;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString);
			deleteStatement.setString(1, name);
			deleteStatement.execute();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "StudentDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
		return ;
	}
	public static void editCustomer(int id, String name, String address, String email, String phone){
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement editStatement = null;
		try {
			editStatement = dbConnection.prepareStatement(editStatementString);
			editStatement.setString(1, name);
			editStatement.setString(2, address);
			editStatement.setString(3, phone);
			editStatement.setString(4, email);
			editStatement.setInt(5, id);
			editStatement.execute();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDAO:edit " + e.getMessage());
		} finally {
			ConnectionFactory.close(editStatement);
			ConnectionFactory.close(dbConnection);
		}
		return ;
	}
}
