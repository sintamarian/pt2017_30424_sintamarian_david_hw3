package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import model.Order;
import model.Product_order;

public class ProductOrderDAO {
	protected static final Logger LOGGER = Logger.getLogger(CustomerDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO product_order (order_id, product_id, quantity)"
			+ " VALUES (?,?, ?)";
	private final static String findByOrderStatementString = "SELECT * FROM product_order where order_id = ?";
	private final static String findByProductStatementString = "SELECT * FROM product_order where product_id = ?";
	private final static String findByProductOrderStatementString = "SELECT * FROM product_order where order_id =? and product_id = ?";
	private final static String getStatementString = "SELECT * FROM order";
	private final static String deleteStatementString = "DELETE FROM order where id = ?";
	public static Product_order findByOrderId(int Orderid){
		Product_order product_order = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findByOrderStatementString);
			findStatement.setInt(1, Orderid);
			rs = findStatement.executeQuery();
			rs.next();
			int id = rs.getInt("id");
			int quantity = rs.getInt("quantity");
			int productId = rs.getInt("product_id");
			product_order = new Product_order(id, Orderid, productId, quantity);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"StudentDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return product_order;
	}
	public static Product_order findByProductId(int productId){
		Product_order product_order = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findByOrderStatementString);
			findStatement.setInt(1, productId);
			rs = findStatement.executeQuery();
			rs.next();
			int id = rs.getInt("id");
			int quantity = rs.getInt("quantity");
			int orderId = rs.getInt("order_id");
			product_order = new Product_order(id, orderId, productId, quantity);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"StudentDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return product_order;
	}
	public static int insertOrder(Product_order productOrder){
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, productOrder.getOrderId());
			insertStatement.setInt(2, productOrder.getProductId());
			insertStatement.setInt(3, productOrder.getQuantity());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "StudentDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
	public static ArrayList<Order> getOrders(){
		ArrayList<Order> orders = new ArrayList<Order>();
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(getStatementString);
			rs = findStatement.executeQuery();
			while(rs.next()){
				int id = rs.getInt("id");
				int customerId = rs.getInt("customer_id");
				int price = rs.getInt("price");
				Order order = new Order(id, customerId, price);
				orders.add(order);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"StudentDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		
		return orders;
	}
	public static void deleteOrder(int id){
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement deleteStatement = null;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString);
			deleteStatement.setInt(1, id);
			deleteStatement.execute();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "StudentDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
		return ;
	}
}
