package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import model.Customer;
import model.Product;

public class ProductDAO {
	protected static final Logger LOGGER = Logger.getLogger(CustomerDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO product (name, price, quantity)"
			+ " VALUES (?,?, ?)";
	private final static String findStatementString = "SELECT * FROM product where name = ?";
	private final static String getStatementString = "SELECT * FROM product";
	private final static String deleteStatementString = "DELETE FROM product where name = ?";
	private final static String editStatementString = "update product set name = ?, price = ?, quantity = ? where id = ?";
	public static Product findByName(String name){
		Product product = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setString(1, name);
			rs = findStatement.executeQuery();
			rs.next();

			int id = rs.getInt("id");
			int price = rs.getInt("price");
			int quantity = rs.getInt("quantity");
			product = new Product(id, name, price, quantity);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"StudentDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return product;
	}
	public static int insertProduct(Product product){
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, product.getName());
			insertStatement.setInt(2, product.getPrice());
			insertStatement.setInt(3, product.getQuantity());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "StudentDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
	public static ArrayList<Product> getProducts(){
		ArrayList<Product> products = new ArrayList<Product>();
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(getStatementString);
			rs = findStatement.executeQuery();
			while(rs.next()){
				int id = rs.getInt("id");
				String name = rs.getString("name");
				int price = rs.getInt("price");
				int quantity = rs.getInt("quantity");
				Product product = new Product(id, name, price, quantity);
				products.add(product);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"StudentDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		
		return products;
	}
	public static void deleteProduct(String name){
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement deleteStatement = null;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString);
			deleteStatement.setString(1, name);
			deleteStatement.execute();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "StudentDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
		return ;
	}
	public static void editProduct(int id, String name, int price, int quantity){
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement editStatement = null;
		try {
			editStatement = dbConnection.prepareStatement(editStatementString);
			editStatement.setString(1, name);
			editStatement.setInt(2, price);
			editStatement.setInt(3, quantity);
			editStatement.setInt(4, id);
			editStatement.execute();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDAO:edit " + e.getMessage());
		} finally {
			ConnectionFactory.close(editStatement);
			ConnectionFactory.close(dbConnection);
		}
		return ;
	}
}
