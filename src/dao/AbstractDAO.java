package dao;

import java.util.logging.Logger;

public class AbstractDAO {
	protected static final Logger LOGGER = Logger.getLogger(CustomerDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO customer (name,address,phone_number,email)"
			+ " VALUES (?,?,?,?)";
	private final static String findStatementString = "SELECT * FROM customer where name = ?";
	private final static String deleteStatementString = "DELETE FROM customer where name = ?";
}
