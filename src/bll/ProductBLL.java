package bll;

import java.util.ArrayList;

import dao.CustomerDAO;
import dao.ProductDAO;
import model.Customer;
import model.Product;

public class ProductBLL {
	public Product getProductByName(String name){
		Product product = ProductDAO.findByName(name);
		return product;
	}
	public ArrayList<Product> getAllCustomers(){
		ArrayList<Product> products = ProductDAO.getProducts();
		return products;
	}
}
