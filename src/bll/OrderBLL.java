package bll;

import dao.OrderDAO;
import model.Order;

public class OrderBLL {
	public int addOrder(int customerId, int price){
		Order order = new Order(customerId, price);
		int createdId = OrderDAO.insertOrder(order);
		return createdId;
	}
}
