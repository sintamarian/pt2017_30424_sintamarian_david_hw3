package bll;

import dao.OrderDAO;
import dao.ProductOrderDAO;
import model.Product_order;

public class ProductOrderBLL {
	public int addProductOrder(int orderId, int productId, int quantity){
		Product_order productOrder = new Product_order(orderId, productId, quantity);
		int id = ProductOrderDAO.insertOrder(productOrder);
		return id;
	}
}
