package bll;

import java.util.ArrayList;

import dao.CustomerDAO;
import model.Customer;

public class CustomerBLL {
	public Customer getCustomerByName(String name){
		Customer customer = CustomerDAO.findByName(name);
		return customer;
	}
	public ArrayList<Customer> getAllCustomers(){
		ArrayList<Customer> customers = CustomerDAO.getCustomers();
		return customers;
	}
}
